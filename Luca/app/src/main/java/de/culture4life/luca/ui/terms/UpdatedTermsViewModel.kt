package de.culture4life.luca.ui.terms

import android.app.Application
import de.culture4life.luca.ui.BaseViewModel

/**
 * Simple ViewModel to access deletion from the BaseViewModel
 */
class UpdatedTermsViewModel(application: Application) : BaseViewModel(application) {
}
