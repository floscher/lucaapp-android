<resources xmlns:xliff="urn:oasis:names:tc:xliff:document:1.2">
    <!-- Global -->
    <string name="app_name" translatable="false">luca</string>
    <string name="unknown">Unknown</string>
    <string name="feature_unavailable">Available soon</string>
    <string name="url_terms_and_conditions">https://www.luca-app.de/app-terms-and-conditions-2/</string>
    <string name="url_privacy_policy">https://www.luca-app.de/app-privacy-policy/</string>
    <string name="url_imprint">https://luca-app.de/imprint</string>
    <string name="url_gitlab" translatable="false">https://gitlab.com/lucaapp/android</string>
    <string name="url_faq">https://www.luca-app.de/faq/</string>
    <string name="mail_support">hello@luca-app.de</string>
    <string name="qr_code">QR Code</string>
    <string name="qr_code_scanner">QR Code Scanner</string>
    <string name="additional_details_content_description">Additional Details</string>

    <!-- Actions -->
    <string name="action_ok">@android:string/ok</string>
    <string name="action_cancel">@android:string/cancel</string>
    <string name="action_yes">Yes</string>
    <string name="action_no">No</string>
    <string name="action_no_thanks">No thanks</string>
    <string name="action_stop">Stop</string>
    <string name="action_retry">Retry</string>
    <string name="action_resolve">Resolve</string>
    <string name="action_continue">Continue</string>
    <string name="action_finish">Finish</string>
    <string name="action_grant">Grant</string>
    <string name="action_settings">Settings</string>
    <string name="action_send">Send</string>
    <string name="action_confirm">Confirm</string>
    <string name="action_update">Update</string>
    <string name="action_enable">Enable</string>
    <string name="action_add">Add</string>

    <!-- Errors -->
    <string name="error_generic_title">Something went wrong</string>
    <string name="error_generic_description">An unexpected error occurred. Please get in touch if this issue persists.</string>
    <string name="error_specific_title">Error: <xliff:g example="HttpException" id="exceptionType">%1$s</xliff:g></string>
    <string name="error_specific_description">An unexpected error occurred:\n\n<xliff:g example="Request not authorized, status code 401" id="exceptionMessage">%1$s</xliff:g>\n\nPlease get in touch if this issue persists.</string>
    <string name="error_request_failed_title">Request failed</string>
    <string name="error_no_network_connection">No network connection</string>
    <string name="error_check_in_failed">Check-in failed</string>
    <string name="error_location_not_found">It is not possible to check in here any more.</string>
    <string name="error_qr_code_broken">QR-Code is invalid. Please try a different one.</string>
    <string name="error_certificate_pinning_title">Unexpected server certificates</string>
    <string name="error_certificate_pinning_description">When attempting to establish a secure connection to the luca servers, we found untrusted certificates.\n\nPlease try switching your network and make sure that you\'re using the latest app version.</string>
    <string name="error_timestamp_offset_title">Wrong phone time</string>
    <string name="error_timestamp_offset_description">We noticed that your phone time is not in sync with our server. Please open your system settings and make sure that your phone time is correct. Using luca with a wrong phone time can lead to unexpected issues.</string>

    <!-- Statuses -->
    <string name="status_check_out_in_progress">Checking out…</string>
    <string name="status_check_out_succeeded">Checked out</string>
    <string name="status_check_out_failed">Checkout failed</string>

    <!-- Notifications -->
    <string name="notification_channel_status_title">Status</string>
    <string name="notification_channel_status_description">Indicates that luca is running.</string>
    <string name="notification_channel_access_title">Data accesses</string>
    <string name="notification_channel_access_description">Informs you if a health department has accessed check-ins related to you.</string>
    <string name="notification_channel_event_title">Information</string>
    <string name="notification_channel_event_description">Informs you about events that happened within luca.</string>
    <string name="notification_service_title">Checked in</string>
    <string name="notification_service_description">@string/notification_checked_in_at_description</string>
    <string name="notification_error_title">luca encountered a problem</string>
    <string name="notification_checked_in_at_title">Checked in at <xliff:g example="neXenio" id="companyName">%1$s</xliff:g></string>
    <string name="notification_checked_in_at_description">You\'re currently checked in. Please remember to check out when leaving the location.</string>
    <string name="notification_meeting_host_title">Private meeting</string>
    <string name="notification_meeting_host_description">You\'re currently hosting a private meeting. Please end it as soon as your guests left.</string>
    <string name="notification_service_error_title">luca is having issues</string>
    <string name="notification_data_accessed_title">@string/accessed_data_dialog_title</string>
    <string name="notification_data_accessed_description">A health department has requested your contact data.</string>
    <string name="notification_auto_checkout_triggered_title">@string/auto_checkout_info_title</string>
    <string name="notification_auto_checkout_triggered_description">You\'ve left the location and have been checked out automatically.</string>
    <string name="notification_action_stop_service">Stop service</string>
    <string name="notification_action_check_out">Check out</string>
    <string name="notification_action_end_meeting">@string/meeting_end_action</string>

    <!-- Welcome -->
    <string name="welcome_app_slogan">Experience life together.</string>
    <string name="welcome_heading">Hello</string>
    <string name="welcome_description">luca helps you encrypt and securely submit your contact data. With luca, you don\'t have to worry about your data when visiting events, restaurants, cafés or bars anymore.</string>
    <string name="welcome_terms_description">Accept <a href="https://www.luca-app.de/app-terms-and-conditions-2/">terms of use</a></string>
    <string name="welcome_terms_checkbox_content_description">Terms of use</string>
    <string name="welcome_privacy_description">Accept <a href="https://www.luca-app.de/app-privacy-policy/">privacy policy</a></string>
    <string name="welcome_privacy_checkbox_content_description">Privacy Policy</string>
    <string name="welcome_action">Get started!</string>

    <!-- Pre-Registration -->
    <string name="pre_registration_heading">Your contact data</string>
    <string name="pre_registration_description">Please enter your contact data in the next step.\n\nYour data will be encrypted. Only a health department can decrypt it during contact tracing.</string>

    <!-- Permissions -->
    <string name="missing_permission_arg">Missing %1$s Permission</string>
    <string name="permission_name_location">Location</string>
    <string name="permission_name_camera">Camera</string>

    <!-- Registration -->
    <string name="registration_first_name">First name</string>
    <string name="registration_last_name">Last name</string>
    <string name="registration_street">Street</string>
    <string name="registration_house_number">Number</string>
    <string name="registration_postal_code">Postal code</string>
    <string name="registration_city_name">City</string>
    <string name="registration_phone_number">Phone number</string>
    <string name="registration_email">Email (optional)</string>
    <string name="registration_heading_name">What\'s your name?</string>
    <string name="registration_heading_address">Where do you live?</string>
    <string name="registration_heading_contact">How can you be contacted?</string>
    <string name="registration_missing_info">Info missing</string>
    <string name="registration_missing_fields_error_text">Please provide your name, phone number and address.</string>
    <string name="registration_will_delete_tests_title">Save changes?</string>
    <string name="registration_will_delete_tests_text">"By changing your name, imported documents will be deleted. Apply all changes?"</string>
    <string name="registration_invalid_value_field_error">This value is not valid</string>
    <string name="registration_empty_but_required_field_error">This field is required</string>
    <string name="registration_missing_first_name_hint">Please enter your first name to continue.</string>
    <string name="registration_missing_last_name_hint">Please enter your last name to continue.</string>
    <string name="registration_missing_phone_number_hint">Please enter your phone number to continue.</string>
    <string name="registration_missing_street_hint">Please enter your street to continue.</string>
    <string name="registration_missing_house_number_hint">Please enter your house number to continue.</string>
    <string name="registration_missing_postal_code_hint">Please enter your zip code to continue.</string>
    <string name="registration_missing_city_hint">Please enter your city to continue.</string>
    <string name="registration_address_mandatory">Hosts must collect the addresses of their guests. Therefore, in order to use luca, it\'s mandatory to enter your address.</string>
    <string name="registration_contact_info">Please provide a correct mobile or landline number. This number will be used for verification. You can choose if you want to provide an email address.</string>
    <string name="registration_address_info">Hosts usually have to collect the addresses of their guests. Therefore, in order to use luca, it\'s mandatory to provide your address.</string>

    <!-- Onboarding -->
    <string name="onboarding_info_heading">Your contact data</string>
    <string name="onboarding_info_description">Please enter your contact data in the next step.\n\nYour data will be encrypted. Only health departments can decrypt it.</string>
    <string name="onboarding_info_action">Got it</string>
    <string name="onboarding_completed_heading">That\'s it!</string>
    <string name="onboarding_completed_description">You can now share your encrypted contact data by presenting your QR code or by scanning a code from a place that uses luca.\n\nChecking in is possible at places that use luca or private meetings with other luca App users.</string>
    <string name="onboarding_completed_action">Done</string>

    <!-- Navigation -->
    <string name="navigation_check_in">Check-In</string>
    <string name="navigation_contact_data">Contact data</string>
    <string name="navigation_venue_list">Venues</string>
    <string name="navigation_venue_details">@string/navigation_venue_list</string>
    <string name="navigation_history">History</string>
    <string name="navigation_meeting">@string/meeting_heading</string>
    <string name="navigation_accessed_data">@string/accessed_data_heading</string>
    <string name="navigation_my_luca">@string/my_luca_heading</string>
    <string name="navigation_account">Account</string>

    <!-- Menu -->
    <string name="menu_more_content_description">More Menu</string>
    <string name="menu_contact_data">Edit Contact Data</string>
    <string name="menu_clear_history">Clear History</string>
    <string name="menu_privacy_policy">Privacy Policy</string>
    <string name="menu_terms_and_conditions">Terms of Use</string>
    <string name="menu_faq">FAQ</string>
    <string name="menu_imprint">Imprint</string>
    <string name="menu_gitlab">GitLab</string>
    <string name="menu_support">Support</string>
    <string name="menu_support_error_title">Unable to send Email</string>
    <string name="menu_support_error_description">It is not possible to send an email from this device because no email account is connected.</string>
    <string name="menu_support_subject">Support Request</string>
    <string name="menu_support_body">Dear luca support,\nThe following problem occurred while using the app:\n\nPlease describe here as precisely as possible your problem and where it occurs. It would be very helpful if you could also attach a screenshot or screen capture of the error message or problem.\n\n<xliff:g example="Google Pixel" id="device_info">%1$s</xliff:g></string>
    <string name="menu_support_device_info">Device: <xliff:g example="Google Pixel" id="device_model">%1$s</xliff:g>\nOperating system: Android <xliff:g example="10" id="android_version_number">%2$s</xliff:g>\nApp version: <xliff:g example="1.0.0" id="version_name">%3$s</xliff:g> (release <xliff:g example="42" id="version_code">%4$d</xliff:g>)</string>
    <string name="menu_app_data">Show App Data</string>
    <string name="menu_version_details">Version Details</string>
    <string name="menu_delete_account">Delete Account</string>

    <!-- Contact Data -->
    <string name="qr_code_description">This is your QR code. For check-in, have it scanned at a place that uses luca.</string>
    <string name="scan_qr_code_description">Scan the QR code of a place that uses luca or a private meeting to check yourself in.</string>
    <string name="scan_qr_code">Self check-in</string>
    <string name="camera_access_title">Enable camera access</string>
    <string name="camera_access_description">luca needs access to your camera so that you can scan QR codes of documents, at locations or private meetings. Only the QR code data will be processed and no other data will be stored.</string>
    <string name="close_qr_code_scanner">Close QR code scanner</string>
    <string name="edit_contact_data">Edit contact data</string>

    <!-- Private Meeting -->
    <string name="meeting_heading">Private Meeting</string>
    <string name="meeting_description">Let your guests scan the QR code to check into your meeting.</string>
    <string name="meeting_description_info">Private meetings are meant for small gatherings with friends and family. They are used as reminders and won\'t be shared with health authorities - that\'s why they display the full names of you and your guests.</string>
    <string name="meeting_duration_heading">Duration</string>
    <string name="meeting_members_heading">Participants</string>
    <string name="meeting_members_info"><xliff:g example="2/4" id="count">%1$s</xliff:g>\n\nChecked in:\n<xliff:g example="- John Doe\n- Jane Doe" id="checkedIn">%2$s</xliff:g>\n\nChecked out:\n<xliff:g example="- John Doe\n- Jane Doe" id="checkedOut">%3$s</xliff:g></string>
    <string name="meeting_create_action">Create a private meeting</string>
    <string name="meeting_create_modal_action">Start</string>
    <string name="meeting_create_modal_heading">Set up a private meeting</string>
    <string name="meeting_create_modal_description">Do you want to start a private meeting now?\n\nWhen friends and family check into your private meeting, their first and last names will be shown in your app. They will also see your first and last name in their app.\n\nPrivate meetings are used as reminders and won\'t be shared with health authorities.</string>
    <string name="meeting_end_confirmation_heading">@string/meeting_end_action</string>
    <string name="meeting_end_confirmation_description">Do you really want to end this meeting?</string>
    <string name="meeting_end_action">End meeting</string>
    <string name="meeting_slider_text">END MEETING</string>
    <string name="meeting_slider_content_description">End meeting button</string>
    <string name="meeting_was_ended_hint">The meeting was ended</string>

    <string name="meeting_join_heading">@string/meeting_heading</string>
    <string name="meeting_join_description">When you check yourself in at a private meeting, your host can see your first and last name.\n\nPrivate meetings are used as reminders and won\'t be shared with health authorities.</string>

    <!-- Venues -->
    <string name="venue_heading">@string/dummy_heading</string>
    <string name="venue_description">You\'re checked in!\n\nCheck-in:\n<xliff:g example="18.08.2020 – 7:23 PM" id="time">%1$s</xliff:g></string>
    <string name="venue_current_stay_timer_heading">Current stay</string>

    <string name="venue_property_table">Table</string>
    <string name="venue_property_patient_name">Patient name</string>

    <string name="venue_check_in_action">CHECK IN</string>
    <string name="venue_check_in_content_description">Check-in button</string>
    <string name="venue_check_out_action">CHECK OUT</string>
    <string name="venue_check_out_content_description">Check-out button</string>
    <string name="venue_checked_in_time">Check in:\n<xliff:g example="18.08.2020 – 7:23 PM" id="time">%1$s</xliff:g></string>
    <string name="venue_automatic_check_out">Automatic Check-Out</string>
    <string name="venue_slider_clicked">Please slide the arrow to the left</string>
    <string name="venue_checked_out">You have checked out</string>

    <string name="venue_children_title">Children</string>
    <string name="venue_children_empty_list_description">You can check your children (up to&#160;14 years) in with you. Please indicate who is accompanying you.</string>
    <string name="venue_children_list_description">Choose the children, who will be checked-in together with you.</string>
    <string name="venue_children_add_description">This information will not be shared with the health department, it is only a reminder for yourself. If you share your history with the health department, you can voluntarily provide this information to them as well.</string>
    <string name="venue_children_add">Add child</string>
    <string name="venue_children_add_validation_error">Please enter a valid name</string>
    <string name="venue_children_add_error_title">Unable to add child</string>
    <string name="venue_children_remove_error_title">Unable to remove child</string>
    <string name="venue_children_name">Child\'s name</string>
    <string name="venue_children_check_in">Check-In Children</string>

    <string name="venue_check_out_error">Checkout failed</string>
    <string name="venue_check_out_error_permission">A required permission has not been granted.</string>
    <string name="venue_check_out_error_duration">You just checked in a moment ago. Please don\'t check out before actually leaving the venue.</string>
    <string name="venue_check_out_error_in_range">You seem to still be at the venue. Please don\'t check out before actually leaving the venue.</string>
    <string name="venue_check_out_error_location_unavailable">We can\'t verify your current location. Please make sure that location services are turned on.</string>
    <string name="venue_check_out_error_generic">Unable to check you out right now. Please make sure that you are connected to the internet.</string>

    <string name="auto_checkout_info_title">Automatic check-out</string>
    <string name="auto_checkout_info_description">Activating this feature will check you out automatically as soon as you leave the location you\'re currently checked in at.\n\nLuca needs to have access to your location for this to work. Checking out, disabling location services or restarting your device will disable this feature.</string>
    <string name="auto_checkout_location_access_title">Enable location access</string>
    <string name="auto_checkout_location_access_description">This automatic checkout feature uses geofencing. If supported by a luca-location, you can check out automatically using you phone\'s location service, even when the app is closed. This is only done with your consent under art. 6 (1) 1 a) GDPR (more: <a href="https://www.luca-app.de/app-privacy-policy/">data privacy - app</a>). However, you can still check out manually at any time.</string>
    <string name="auto_checkout_background_location_access_title">@string/auto_checkout_location_access_title</string>
    <string name="auto_checkout_background_location_access_description">For the automatic check-out feature, luca needs to have access to your location while you\'re checked in, even when the app is closed. Please select the following option: <xliff:g example="Allow all the time" id="locationPermissionOption">%1$s</xliff:g></string>
    <string name="auto_checkout_enable_location_title">Enable location services</string>
    <string name="auto_checkout_enable_location_description">Please activate your location services to use this feature.</string>
    <string name="auto_checkout_location_disabled_title">Automatic check-out disabled</string>
    <string name="auto_checkout_location_disabled_description">Automatic check-out is not possible anymore because your location services have been disabled.</string>
    <string name="auto_checkout_generic_error_title">Automatic check-out error</string>
    <string name="auto_checkout_generic_error_description">Unable to activate automatic check-out.</string>

    <!-- History -->
    <string name="history_empty_title">No entries</string>
    <string name="history_empty_description">Your history collects all your activities from the last 14&#160;days. You don\'t have any entries during this period at the moment.</string>
    <string name="history_requests"><xliff:g example="0" id="count">%1$d</xliff:g> data requests</string>
    <string name="history_time"><xliff:g example="18.08.2020 – 19:23" id="time">%1$s</xliff:g></string>
    <string name="history_time_merged" translatable="false"><xliff:g example="18.08.2020 – 19:23" id="startTime">%1$s</xliff:g> -\n<xliff:g example="18.08.2020 – 20:11" id="endTime">%2$s</xliff:g></string>
    <string name="history_share">Share data</string>
    <string name="history_share_selection_title">Define timeframe</string>
    <string name="history_share_selection_description">Your history gathers all of your check-ins from the last 14&#160;days. You can decide how many days you want to share with the health department. Define the number of days:</string>
    <string name="history_share_selection_action">Select</string>
    <string name="history_share_selection_hint">Days</string>
    <string name="history_share_confirmation_title">@string/history_share</string>
    <string name="history_share_confirmation_description">Would you like to share your contact data and the last <xliff:g example="14" id="count">%1$d</xliff:g>&#160;days of your history with the health department? This is only done with your voluntary consent in accordance with art. 9 (2) a) GDPR (more: <a href="https://www.luca-app.de/app-privacy-policy/">data privacy - app</a>).</string>
    <string name="history_share_confirmation_action">Share</string>
    <string name="history_share_tan_title">@string/history_share</string>
    <string name="history_share_tan_description">If you give this TAN to a health department, they can decrypt your history and inform luca places:\n\n<xliff:g example="VU5S-86V6-H8FV" id="tan">%1$s</xliff:g> </string>
    <string name="history_contact_data_update_title">Contact data updated</string>
    <string name="history_contact_data_update_description"><xliff:g example="John Doe" id="userName">%1$s</xliff:g></string>
    <string name="history_check_in_title">Check-In</string>
    <string name="history_check_in_description"><xliff:g example="neXenio Office" id="locationName">%1$s</xliff:g></string>
    <string name="history_check_out_title">Check-Out</string>
    <string name="history_check_out_description"><xliff:g example="neXenio Office" id="locationName">%1$s</xliff:g></string>
    <string name="history_meeting_started_title">@string/history_meeting_ended_title</string>
    <string name="history_meeting_ended_title">Private meeting</string>
    <string name="history_meeting_ended_description"><xliff:g example="- John Doe\n- Jane Doe" id="members">%1$s</xliff:g></string>
    <string name="history_meeting_not_empty_description">Guests: <xliff:g example="3" id="guestsCount">%1$d</xliff:g></string>
    <string name="history_meeting_empty_description">No guests checked in</string>
    <string name="history_data_cleared_title">History cleared</string>
    <string name="history_data_cleared_description">All entries have been removed from this device.</string>
    <string name="history_data_accessed_details">While contact tracing, a health department has accessed the data from this check-in.</string>
    <string name="history_data_shared_title">Data shared</string>
    <string name="history_data_shared_description">You released your contact data and the last <xliff:g example="14" id="count">%1$d</xliff:g>&#160;days of your history for the health authorities.</string>
    <string name="history_clear_title">Do you really want to clear your history?</string>
    <string name="history_clear_description">Past entries won\'t be displayed in your app anymore, but will stay in the system for up to 30&#160;days and will be shared with the health authorities if you share your data.</string>
    <string name="history_clear_action">Clear</string>
    <string name="history_children_title">Children: <xliff:g example="1" id="count">%1$d</xliff:g></string>
    <string name="history_children_description" translatable="false"><xliff:g example="- John Doe\n- Jane Doe" id="members">%1$s</xliff:g></string>


    <!-- Accessed Data -->
    <string name="accessed_data_heading">Data requests</string>
    <string name="accessed_data_empty_title">@string/history_empty_title</string>
    <string name="accessed_data_empty_description">No data requests from health departments in the last 14&#160;days.</string>
    <string name="accessed_data_title"><xliff:g example="Health department Berlin Mitte" id="healthDepartmentName">%1$s</xliff:g></string>
    <string name="accessed_data_description"><xliff:g example="neXenio Office" id="locationName">%1$s</xliff:g></string>
    <string name="accessed_data_time" translatable="false"><xliff:g example="18.08.2020 – 19:23" id="startTime">%1$s</xliff:g> -\n<xliff:g example="18.08.2020 – 20:11" id="endTime">%2$s</xliff:g></string>
    <string name="accessed_data_dialog_title">New data request</string>
    <string name="accessed_data_dialog_description">One or more health departments have requested your contact data.\n\nWhile contact tracing, one or more health departments decrypted your contact data. They are currently evaluating whether there is a risk and will contact you if necessary.\n\nPlease act responsibly.</string>
    <string name="accessed_data_dialog_action_show">All data requests</string>
    <string name="accessed_data_dialog_action_dismiss">@string/action_ok</string>

    <!-- My Luca -->
    <string name="my_luca_heading">My luca</string>
    <string name="my_luca_empty_title">Welcome at \"My luca\"</string>
    <string name="my_luca_empty_description">This is your personal space, where you can <b>add</b> documents to make your <b>check-in</b> easier.\n\nVia the calendar icon in the upper right corner you can book test appointments.</string>
    <string name="time_error_description">The time you set in your phone doesn\'t match the current time.</string>
    <string name="time_error_action">Go to settings</string>
    <string name="appointment_action">Book appointment</string>
    <string name="document_import_action">Add Document</string>
    <string name="document_import_hint">Scan the QR code of your document to add it to the app.</string>
    <string name="document_import_success_message">The document was imported</string>
    <string name="document_import_consent">I hereby declare my consent in accordance with Art. 9 (2) a) in conjunction with Art. 6 (1) 1 a) DSGVO to the transmission of a pseudonymized identifier during the local storage of my COVID test, recovery or vaccination certificate in the luca app. This is solely to prevent misuse so that multiple people cannot import and use the same document. The document\’s individual identifier is automatically deleted from the luca system after 72 hours of storage.</string>
    <string name="document_import_error_title">The document could not be imported</string>
    <string name="document_import_error_name_mismatch_description">It was not possible to validate that this document belongs to you. Please check if your name is entered correctly in the app.</string>
    <string name="document_import_error_invalid_signature_description">It was not possible to validate the signature of this document. The document provider may not be supported by luca yet.</string>
    <string name="document_import_error_expired_description">Unfortunately, the document cannot be imported because it already expired.</string>
    <string name="document_import_error_already_imported_description">This document was already imported.</string>
    <string name="document_import_error_not_negative_title">Adding not possible</string>
    <string name="document_import_error_not_negative_description">luca helps you to check in easily. Since only negative test results are relevant for checking in, you can only add negative results at the moment. You can only add a positive result if it is a PCR test older than 14 days.</string>
    <string name="document_import_error_unsupported_description">The QR code could not be scanned and the data could not be processed. This could be because this QR code is not compatible with luca.</string>
    <string name="document_import_error_unsupported_but_url_description">The data didn\'t contain a valid document from a provider that we support.\n\nHowever, it seems to point to a website where you may get more details.</string>
    <string name="document_import_error_check_in_scanner_title">You cannot check in here</string>
    <string name="document_import_error_check_in_scanner_description">This scanner is only for adding documents. If you want to check in at a location, switch to the \"Check-In\" tab (bottom left of the navigation bar). There you will find the \"Self Check-In\" button.</string>
    <string name="document_outcome_negative">RESULT: Negative</string>
    <string name="document_outcome_positive">RESULT: Positive</string>
    <string name="document_outcome_partially_immune">Partially Immune %1$s</string>
    <string name="document_outcome_partially_vaccinated">Partial vaccination %1$s</string>
    <string name="document_outcome_fully_vaccinated">Complete vaccination %1$s</string>
    <string name="document_outcome_fully_vaccinated_in">Complete vaccination in %1$s</string>
    <string name="document_outcome_fully_recovered">RECOVERY PROOF</string>
    <string name="document_outcome_fully_recovered_in">Recovered in %1$s</string>
    <string name="document_outcome_unknown">Unknown</string>
    <string name="document_type_fast">Rapid Test</string>
    <string name="document_type_pcr">PCR Test</string>
    <string name="document_type_vaccination">VACCINATION CERTIFICATE</string>
    <string name="document_type_unknown">@string/document_outcome_unknown</string>
    <string name="document_type_of_document_label">Type:</string>
    <string name="document_issued_by">Issued:</string>
    <string name="vaccination_date_label">Vaccination date:</string>
    <string name="document_issued_at">Date:</string>
    <string name="document_created_before">Created before:</string>
    <string name="document_valid_until">Valid until:</string>
    <string name="document_lab_issuer">Performed by:</string>
    <string name="document_doctor_title">Doctor:</string>
    <string name="document_result_time" translatable="false"><xliff:g example="18.08.2020 – 19:23" id="time">%1$s</xliff:g></string>
    <string name="document_vaccination_procedure">%1$s. Vaccination:</string>
    <string name="document_recovery_procedure">Infection %1$s - %2$s</string>
    <string name="item_delete_action">Delete</string>
    <string name="delete_test_action">Delete test</string>
    <string name="delete_appointment_action">Delete appointment</string>
    <string name="delete_certificate_action">Delete certificate</string>
    <string name="document_delete_confirmation_message">Do you really want to delete the document?</string>
    <string name="vaccine_comirnaty">BioNTech/Pfizer</string>
    <string name="vaccine_jannsen">Johnson &amp; Johnson</string>
    <string name="vaccine_moderna">Moderna</string>
    <string name="vaccine_vaxzevria">AstraZeneca</string>
    <string name="vaccine_sputnik">Sputnik V</string>
    <string name="procedure_recovery">Recovery</string>
    <string name="em_green_pass_valid">Valid on:</string>
    <string name="em_green_pass_status">Status:</string>
    <string name="appointment_title">APPOINTMENT: <xliff:g example="Rapid Test" id="type">%1$s</xliff:g></string>
    <string name="appointment_address">Address:</string>
    <string name="birthday_label">Date of birth:</string>
    <string name="provider_name_eu_dcc">EU Digital COVID Certificate</string>

    <!-- Phone Number Verification -->
    <string name="verification_request_new_tan_action">Request new TAN</string>
    <string name="verification_explanation_title">Verify phone number</string>
    <string name="verification_explanation_sms_description">To verify your phone number we will send a TAN to the number you provided in the next step. Please make sure this number is correct:\n\n<b><xliff:g example="+4917600000000" id="phoneNumber">%1$s</xliff:g></b></string>
    <string name="verification_explanation_landline_description">You will receive a call with a message containing the TAN that is needed to verify your phone number in the next step. Please make sure the phone is nearby and the number is correct:\n\n<b><xliff:g example="+4917600000000" id="phoneNumber">%1$s</xliff:g></b></string>
    <string name="verification_enter_tan_title">@string/verification_explanation_title</string>
    <string name="verification_enter_tan_description">We\'ve sent you a message with a TAN. Please enter the TAN here to verify your phone number.</string>
    <string name="verification_enter_tan_error">The TAN you entered was incorrect. Please try again.</string>
    <string name="verification_tan_delayed_action">I didn\'t get a TAN</string>
    <string name="verification_tan_delayed_title">@string/verification_explanation_title</string>
    <string name="verification_tan_delayed_description">It can take a few minutes for the TAN to arrive. Please wait a moment before you request another TAN. If nothing happens after a few minutes, please check if you\'ve provided the correct number and try again.</string>
    <string name="verification_timeout_error_title">TAN already requested</string>
    <string name="verification_timeout_error_description">You have recently requested a TAN. Please wait a moment. If nothing happens, you can try again in:\n<xliff:g example="1 minute" id="timeoutDuration">%1$s</xliff:g></string>
    <string name="verification_timeout_action_use_last">Enter last TAN</string>
    <string name="verification_rate_limit_title">Rate limit exceeded</string>
    <string name="verification_rate_limit_description">This network\'s TAN request limit has been reached. Please try to switch your network or try again at a later time.</string>
    <string name="verification_bad_gateway_title">Service unavailable</string>
    <string name="verification_bad_gateway_description">The luca service is currently unavailable, please try again later.</string>

    <!-- Update -->
    <string name="update_required_title">Update required</string>
    <string name="update_required_description">The luca app that you are currently using is outdated, please install the latest version.</string>

    <!-- Time -->
    <string name="time_format">dd.MM.yyyy h:mm a</string>
    <string name="date_format">dd.MM.yyyy</string>
    <string name="time_seconds">seconds</string>
    <string name="time_minutes">minutes</string>
    <string name="time_hours">hours</string>
    <string name="time_days">days</string>
    <plurals name="time_plural_seconds">
        <item quantity="one" formatted="true">%d second</item>
        <item quantity="other" formatted="true">%d seconds</item>
    </plurals>
    <plurals name="time_plural_minutes">
        <item quantity="one" formatted="true">%d minute</item>
        <item quantity="other" formatted="true">%d minutes</item>
    </plurals>
    <plurals name="time_plural_hours">
        <item quantity="one" formatted="true">%d hour</item>
        <item quantity="other" formatted="true">%d hours</item>
    </plurals>
    <plurals name="time_plural_days">
        <item quantity="one" formatted="true">%d day</item>
        <item quantity="other" formatted="true">%d days</item>
    </plurals>

    <!-- Version details -->
    <string name="version_details_dialog_title">App Version</string>
    <string name="version_details_dialog_message">You are currently using the Android app version <xliff:g example="1.0.0" id="version_name">%1$s</xliff:g> (release <xliff:g example="42" id="version_code">%2$d</xliff:g>).\n\nThe source code can be found on GitLab using the commit hash <xliff:g example="843cbaf2d9840624defe303c0c854385ee69d23f" id="commit_hash">%3$s</xliff:g>.</string>
    <string name="version_details_dialog_action">@string/action_ok</string>

    <!-- Account deletion -->
    <string name="delete_account_dialog_title">Do you really want to delete your account?</string>
    <string name="delete_account_dialog_message">If you delete your account, you will not be able to check in or access your history.\n\nIn accordance with the Corona/COVID-19 infection control regulations, all encrypted check-in data will be deleted after 4 weeks.\n\nOnce your account is deleted, you can still be notified by a health department up to 4 weeks after your last check-in.</string>
    <string name="delete_account_dialog_action">Delete</string>

    <!-- Updated Terms -->
    <string name="updated_terms_description">We are updating our terms of use and privacy policy.</string>
    <string name="updated_terms_info">Tap "AGREE" to accept the updated <b><a href="https://www.luca-app.de/app-terms-and-conditions-2/">terms of use</a></b> and <b><a href="https://www.luca-app.de/app-privacy-policy/">privacy policy</a></b>.</string>
    <string name="updated_terms_button_agree">AGREE</string>
    <string name="updated_terms_changes_action">Changes</string>
    <string name="updated_terms_changes_url" translatable="false">https://www.luca-app.de/changes-terms-of-use-app/</string>

    <!-- Dummy Data -->
    <string name="dummy_heading" translatable="false">Lorem Ipsum</string>
    <string name="dummy_sentence" translatable="false">Quisque hendrerit facilisis ultricies, fusce a convallis.</string>
    <string name="dummy_paragraph" translatable="false">Curabitur tincidunt in lectus at scelerisque. Nunc arcu lectus, lobortis ut aliquam ac, accumsan nec risus. Quisque hendrerit facilisis ultricies. Fusce a convallis lacus.</string>

</resources>